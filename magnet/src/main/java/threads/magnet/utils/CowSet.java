package threads.magnet.utils;

import androidx.annotation.NonNull;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.function.Function;
import java.util.stream.Stream;

public class CowSet<Key> {

    @SuppressWarnings("rawtypes")
    private static final AtomicReferenceFieldUpdater<CowSet, HashMap> u =
            AtomicReferenceFieldUpdater.newUpdater(CowSet.class, HashMap.class, "backingStore");

    private volatile HashMap<Key, Boolean> backingStore = new HashMap<>();

    private <T> T update(Function<HashMap<Key, Boolean>, ? extends T> c) {
        HashMap<Key, Boolean> current;
        final HashMap<Key, Boolean> newMap = new HashMap<>();
        T ret;

        do {
            //noinspection unchecked
            current = (HashMap<Key, Boolean>) u.get(this);
            newMap.clear();
            newMap.putAll(current);
            ret = c.apply(newMap);
        } while (!u.compareAndSet(this, current, newMap));

        return ret;
    }


    public boolean isEmpty() {
        return backingStore.isEmpty();
    }


    public boolean contains(Key o) {
        return backingStore.containsKey(o);
    }


    public boolean add(Key e) {
        if (backingStore.containsKey(e))
            return false;
        return update(m -> m.putIfAbsent(e, Boolean.TRUE) == null);
    }


    public void remove(Key o) {
        if (!backingStore.containsKey(o))
            return;
        update(m -> m.keySet().remove(o));
    }


    public void addAll(@NonNull Collection<? extends Key> c) {
        update(m -> {
            boolean added = false;
            for (Key e : c) {
                added |= m.put(e, Boolean.TRUE) == null;
            }
            return added;
        });
    }

    @NonNull
    public Stream<Key> stream() {
        return backingStore.keySet().stream();
    }

    public Set<Key> snapshot() {
        return Collections.unmodifiableSet(backingStore.keySet());
    }

}
