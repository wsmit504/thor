package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import threads.lite.cid.Cid;
import threads.lite.cid.Prefix;
import threads.lite.core.TimeoutCloseable;
import threads.lite.host.Session;
import threads.lite.ipns.Ipns;


@RunWith(AndroidJUnit4.class)
public class IpfsLoadContentTest {
    private static final String TAG = IpfsLoadContentTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void find_peer_freedomReport() throws IOException, InterruptedException {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            //Freedom Report ipns://k51qzi5uqu5dlnwjrnyyd6sl2i729d8qjv1bchfqpmgfeu8jn1w1p4q9x9uqit

            String key = "k51qzi5uqu5dlnwjrnyyd6sl2i729d8qjv1bchfqpmgfeu8jn1w1p4q9x9uqit";

            Ipns.Entry res = ipfs.resolveName(key, 0, new TimeoutCloseable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.toString());

            assertTrue(ipfs.isValid(res.getHash()));
            Cid cid = Cid.decode(res.getHash());
            assertEquals(cid.getVersion(), 0);
            assertEquals(res.getHash(), cid.String());

            Prefix prefix = cid.getPrefix();
            LogUtils.debug(TAG, prefix.toString());
            assertTrue(prefix.isSha2556());

            Cid root = ipfs.resolve(session, cid, Collections.emptyList(), new TimeoutCloseable(60));
            assertNotNull(root);
            assertEquals(cid, root);

            Cid node = ipfs.resolve(session, cid, List.of(IPFS.INDEX_HTML), new TimeoutCloseable(60));
            assertNotNull(node);

            String text = ipfs.getText(session, node, new TimeoutCloseable(30));

            assertNotNull(text);
            TestCase.assertFalse(text.isEmpty());
        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_corbett() throws IOException, InterruptedException {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            //CorbettReport ipns://k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm

            String key = "k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm";

            Ipns.Entry res = ipfs.resolveName(key, 0, new TimeoutCloseable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.toString());

            assertTrue(ipfs.isValid(res.getHash()));
            Cid cid = Cid.decode(res.getHash());
            assertEquals(cid.getVersion(), 0);
            assertEquals(res.getHash(), cid.String());

            Prefix prefix = cid.getPrefix();
            LogUtils.debug(TAG, prefix.toString());
            assertTrue(prefix.isSha2556());

            Cid node = ipfs.resolve(session, cid, List.of(IPFS.INDEX_HTML), new TimeoutCloseable(60));
            assertNotNull(node);

            String text = ipfs.getText(session, node, new TimeoutCloseable(30));

            assertNotNull(text);
            TestCase.assertFalse(text.isEmpty());
        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_freedom() throws IOException, InterruptedException {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            //FreedomsPhoenix.com ipns://k2k4r8magsykrprepvtuvd1h8wonxy7rbdkxd09aalsvclqh7wpb28m1

            String key = "k2k4r8magsykrprepvtuvd1h8wonxy7rbdkxd09aalsvclqh7wpb28m1";

            Ipns.Entry res = ipfs.resolveName(key, 0, new TimeoutCloseable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.toString());

            assertTrue(ipfs.isValid(res.getHash()));
            Cid cid = Cid.decode(res.getHash());
            assertEquals(cid.getVersion(), 0);
            assertEquals(res.getHash(), cid.String());

            Prefix prefix = cid.getPrefix();
            LogUtils.debug(TAG, prefix.toString());
            assertTrue(prefix.isSha2556());

            Cid node = ipfs.resolve(session, cid, List.of(IPFS.INDEX_HTML), new TimeoutCloseable(60));
            assertNotNull(node);

            String text = ipfs.getText(session, node, new TimeoutCloseable(30));

            assertNotNull(text);
            TestCase.assertFalse(text.isEmpty());
        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_pirates() throws IOException, InterruptedException {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {

            //PiratesWithoutBorders.com ipns://k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0zn6t

            String key = "k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0zn6t";

            Ipns.Entry res = ipfs.resolveName(key, 0, new TimeoutCloseable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.toString());

            assertTrue(ipfs.isValid(res.getHash()));
            Cid cid = Cid.decode(res.getHash());
            assertEquals(cid.getVersion(), 0);
            assertEquals(res.getHash(), cid.String());

            Prefix prefix = cid.getPrefix();
            LogUtils.debug(TAG, prefix.toString());
            assertTrue(prefix.isSha2556());

            Cid node = ipfs.resolve(session, cid, List.of(IPFS.INDEX_HTML), new TimeoutCloseable(60));
            assertNotNull(node);

            String text = ipfs.getText(session, node, new TimeoutCloseable(30));

            assertNotNull(text);
            TestCase.assertFalse(text.isEmpty());
        } finally {
            session.clear(true);
        }
    }
}
