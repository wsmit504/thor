package threads.lite.relay;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.host.Dialer;
import threads.lite.host.LiteHost;
import threads.lite.host.Session;
import threads.lite.host.StreamData;
import threads.lite.host.StreamDataHandler;
import threads.lite.utils.DataHandler;

public class RelayService {

    public static final String TAG = RelayService.class.getSimpleName();

    @NonNull
    public static RelayConnection createRelayConnection(@NonNull LiteHost liteHost,
                                                        @Nullable Session session,
                                                        @NonNull PeerId peerId,
                                                        @NonNull PeerId relayId,
                                                        @NonNull Multiaddr multiaddr,
                                                        int timeout,
                                                        int maxIdleTimeoutInSeconds,
                                                        int initialMaxStreams,
                                                        int initialMaxStreamData) throws Exception {

        String host = multiaddr.getHost();
        int port = multiaddr.getPort();

        Multiaddr relayAddr = Multiaddr.transform(new InetSocketAddress(host, port));

        // keepAlive is set to false, only for own relays we are
        // keeping the connection
        QuicConnection conn = Dialer.dial(liteHost, session, relayId, relayAddr, timeout,
                maxIdleTimeoutInSeconds, initialMaxStreams,
                initialMaxStreamData);
        Objects.requireNonNull(conn);


        return RelayConnection.createRelayConnection(conn, peerId);
    }

    @NonNull
    public static Circuit.Reservation reserve(@NonNull QuicConnection conn) throws Exception {

        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setType(Circuit.HopMessage.Type.RESERVE).build();

        long time = System.currentTimeMillis();

        CompletableFuture<Circuit.HopMessage> done = new CompletableFuture<>();
        conn.createStream(new StreamDataHandler(new StreamData() {
            @Override
            public void throwable(Throwable throwable) {
                done.completeExceptionally(throwable);
                conn.close();
            }

            @Override
            public void token(String token) throws Exception {
                if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP).contains(token)) {
                    throw new Exception("Token " + token + " not supported");
                }
            }

            @Override
            public void data(ByteBuffer data) throws Exception {
                done.complete(Circuit.HopMessage.parseFrom(data.array()));
            }

        }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS).thenApply(
                quicStream -> quicStream.writeOutput(
                                DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP))
                        .thenApply(stream -> stream.writeOutput(DataHandler.encode(message))
                                .thenApply(QuicStream::closeOutput)));

        Circuit.HopMessage msg = done.get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

        LogUtils.info(TAG, "Request took " + (System.currentTimeMillis() - time));
        Objects.requireNonNull(msg);

        if (msg.getType() == Circuit.HopMessage.Type.STATUS) {
            if (msg.getStatus() == Circuit.Status.OK) {
                return msg.getReservation();
            }
        }
        throw new Exception("Reservation failed");

    }


    @NonNull
    public static QuicStream getStream(@NonNull QuicConnection conn, @NonNull PeerId peerId,
                                       long timeout, @NonNull TimeUnit timeoutUnit)
            throws Exception {

        Circuit.Peer dest = Circuit.Peer.newBuilder()
                .setId(ByteString.copyFrom(peerId.getBytes())).build();

        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setType(Circuit.HopMessage.Type.CONNECT)
                .setPeer(dest)
                .build();

        long time = System.currentTimeMillis();

        CompletableFuture<Boolean> done = new CompletableFuture<>();
        CompletableFuture<QuicStream> result = conn.createStream(
                new StreamDataHandler(new StreamData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        done.completeExceptionally(throwable);
                        conn.close();
                    }

                    @Override
                    public void token(String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                    }

                    @Override
                    public void data(ByteBuffer data) throws Exception {
                        Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data.array());

                        LogUtils.info(TAG, "Request took " + (System.currentTimeMillis() - time));
                        Objects.requireNonNull(msg);


                        if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                            throw new ConnectException(msg.getType().name());
                        }

                        if (msg.getStatus() != Circuit.Status.OK) {
                            throw new ConnectException(msg.getStatus().name());
                        }

                        if (msg.hasLimit()) {
                            Circuit.Limit limit = msg.getLimit();
                            if (limit.hasData()) {
                                LogUtils.debug(TAG, "Relay Limit Data " + limit.getData());
                            }
                            if (limit.hasDuration()) {
                                LogUtils.debug(TAG, "Relay Limit Duration " +
                                        limit.getDuration());
                            }
                        }
                        LogUtils.debug(TAG, "Success Relay Stream to " + peerId.toBase58());
                        done.complete(true);
                    }

                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

        result.thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP))
                .thenApply(stream -> stream.writeOutput(DataHandler.encode(message))));

        try {
            done.get(timeout, timeoutUnit);
        } catch (Throwable throwable) {
            result.get().closeOutput();
            throw throwable;
        }

        return result.get();

    }

}
