package threads.lite.cid;

import androidx.annotation.NonNull;

import java.util.Set;
import java.util.TreeSet;

public final class Peer implements Comparable<Peer> {
    private final PeerId peerId;
    private final Set<Multiaddr> multiaddrs;
    private final ID id;
    private long latency = Long.MAX_VALUE;  // temporary value
    private boolean replaceable = true; // temporary value

    public Peer(@NonNull PeerId peerId, @NonNull Multiaddr multiaddr) {
        this.peerId = peerId;
        this.id = ID.convertPeerID(peerId);
        this.multiaddrs = new TreeSet<>();
        this.multiaddrs.add(multiaddr);
    }

    public Peer(@NonNull PeerId peerId, @NonNull Set<Multiaddr> multiaddrs) {
        this.peerId = peerId;
        this.id = ID.convertPeerID(peerId);
        this.multiaddrs = multiaddrs;
    }

    public ID getID() {
        return id;
    }

    public long getLatency() {
        return latency;
    }

    public void setLatency(long latency) {
        this.latency = latency;
    }

    public PeerId getPeerId() {
        return peerId;
    }

    public Set<Multiaddr> getMultiaddrs() {
        return multiaddrs;
    }

    public boolean hasAddresses() {
        return !multiaddrs.isEmpty();
    }

    @NonNull
    @Override
    public String toString() {
        return "Peer{" +
                "peerId=" + peerId +
                ", multiaddrs=" + multiaddrs +
                ", latency=" + latency +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peer peer = (Peer) o;
        return id.equals(peer.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public boolean isReplaceable() {
        return replaceable;
    }

    public void setReplaceable(boolean replaceable) {
        this.replaceable = replaceable;
    }

    @Override
    public int compareTo(Peer peer) {
        return id.compareTo(peer.id);
    }
}
