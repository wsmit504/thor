package threads.lite.push;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.TokenData;
import threads.lite.utils.DataHandler;

public class PushService {
    private static final String TAG = PushService.class.getSimpleName();

    public static void notify(@NonNull QuicConnection conn, @NonNull String content) {
        long time = System.currentTimeMillis();

        try {
            conn.createStream(new StreamDataHandler(new TokenData() {
                        @Override
                        public void throwable(Throwable throwable) {
                            LogUtils.error(TAG, throwable); // todo test it with some who do not have it
                        }

                        @Override
                        public void token(String token) throws Exception {
                            if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL).contains(token)) {
                                throw new Exception("Token " + token + " not supported");
                            }
                        }
                    }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .thenApply(quicStream -> quicStream.writeOutput(
                                    DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL))
                            .thenApply(stream -> stream.writeOutput(
                                            DataHandler.encode(content.getBytes()))
                                    .thenApply(QuicStream::closeOutput)));
        } finally {
            LogUtils.debug(TAG, "Send took " + (System.currentTimeMillis() - time));
        }
    }
}
