package threads.lite.dht;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import threads.lite.LogUtils;
import threads.lite.cid.Peer;
import threads.lite.core.Closeable;

public class Query {

    private static final String TAG = Query.class.getSimpleName();

    public static void runQuery(@NonNull KadDht dht, @NonNull Closeable closeable,
                                @NonNull byte[] key, @NonNull List<Peer> seedPeers,
                                @NonNull KadDht.QueryFunc queryFn)
            throws InterruptedException {

        QueryPeerSet queryPeers = QueryPeerSet.create(key);

        ExecutorService executor =
                Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        LinkedBlockingQueue<List<Peer>> queue = new LinkedBlockingQueue<>();
        queue.offer(seedPeers);

        try {
            while (true) {

                List<Peer> current = queue.take();

                if (closeable.isClosed()) {
                    throw new InterruptedException();
                }

                // the peers in query update are added to the queryPeers
                for (Peer peer : current) {
                    if (Objects.equals(peer.getPeerId(), dht.self)) { // don't add self.
                        continue;
                    }
                    queryPeers.tryAdd(peer);  // set initial state to PeerHeard
                }

                boolean result = queryPeers.numWaitingOrHeard() == 0 && queue.isEmpty();
                if (result) {
                    break;
                }

                List<Peer> nextPeersToQuery = queryPeers.nextHeardPeers();

                // try spawning the queries, if there are no available peers to query then we won't spawn them
                for (Peer queryPeer : nextPeersToQuery) {
                    queryPeers.setState(queryPeer, PeerState.PeerWaiting);
                    executor.execute(() -> {
                        try {
                            List<Peer> newPeers = queryFn.query(closeable, queryPeer);

                            // query successful, try to add to routing table
                            dht.addToRouting(queryPeer);
                            queryPeers.setState(queryPeer, PeerState.PeerQueried);
                            queue.offer(newPeers);
                        } catch (InterruptedException interruptedException) {
                            queue.clear();
                            queue.offer(Collections.emptyList());
                        } catch (ConnectException ignore) {
                            dht.removeFromRouting(queryPeer);
                            queryPeers.setState(queryPeer, PeerState.PeerUnreachable);
                            queue.offer(Collections.emptyList());
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                            dht.removeFromRouting(queryPeer);
                            queryPeers.setState(queryPeer, PeerState.PeerUnreachable);
                            queue.offer(Collections.emptyList());
                        }
                    });
                }
            }
            executor.shutdown();
            // wait until all threads are finished
            boolean result = executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
            if (!result) {
                executor.shutdownNow();
            }
            LogUtils.error(TAG, "Starvation Termination " + queryPeers.size());
        } finally {
            executor.shutdownNow();
        }
    }

}
