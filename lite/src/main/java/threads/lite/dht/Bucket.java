package threads.lite.dht;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Peer;


public class Bucket {

    private final Set<Peer> peers = ConcurrentHashMap.newKeySet();

    public boolean containsPeer(@NonNull Peer peer) {
        return peers.contains(peer);
    }

    @Nullable
    public Peer weakest() {

        long latency = 0;
        Peer found = null;
        for (Peer peer : peers) {

            if (peer.isReplaceable()) {
                long tmp = peer.getLatency();

                if (tmp >= latency) {
                    latency = tmp;
                    found = peer;
                }

                if (tmp == Long.MAX_VALUE) {
                    break;
                }
            }
        }
        return found;
    }


    @NonNull
    @Override
    public String toString() {
        return "Bucket{" + "peers=" + size() + '}';
    }

    public void addPeer(@NonNull Peer peer) {
        peers.add(peer);
    }

    public boolean removePeer(@NonNull Peer p) {
        if (p.isReplaceable()) {
            return peers.remove(p);
        }
        return false;
    }

    public int size() {
        return peers.size();
    }

    public Set<Peer> peers() {
        return peers;
    }
}
