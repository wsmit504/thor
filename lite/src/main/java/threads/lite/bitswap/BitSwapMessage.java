package threads.lite.bitswap;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bitswap.pb.MessageOuterClass;
import threads.lite.cid.Cid;
import threads.lite.cid.Prefix;
import threads.lite.format.Block;


public final class BitSwapMessage {

    private final HashMap<Cid, Entry> wantlist = new HashMap<>();
    private final HashMap<Cid, Block> blocks = new HashMap<>();
    private final HashMap<Cid, MessageOuterClass.Message.BlockPresenceType> blockPresences = new HashMap<>();
    private final boolean full;
    private int pendingBytes;

    public BitSwapMessage(boolean full) {
        this.full = full;
    }

    public static BitSwapMessage create(boolean full) {
        return new BitSwapMessage(full);
    }

    public static BitSwapMessage newMessageFromProto(MessageOuterClass.Message pbm)
            throws IOException, NoSuchAlgorithmException {
        BitSwapMessage m = new BitSwapMessage(pbm.getWantlist().getFull());
        for (MessageOuterClass.Message.Wantlist.Entry e :
                pbm.getWantlist().getEntriesList()) {
            Cid cid = new Cid(e.getBlock().toByteArray());
            if (!cid.isDefined()) {
                throw new RuntimeException("errCidMissing");
            }
            m.addEntry(cid, e.getPriority(), e.getCancel(), e.getWantType(), e.getSendDontHave());
        }
        // deprecated
        for (ByteString data : pbm.getBlocksList()) {
            // CIDv0, sha256, protobuf only
            Block block = Block.createBlock(data.toByteArray());
            m.addBlock(block);
        }
        for (MessageOuterClass.Message.Block b : pbm.getPayloadList()) {
            ByteString prefix = b.getPrefix();
            Prefix pref = Prefix.getPrefixFromBytes(prefix.toByteArray());
            Cid cid = pref.sum(b.getData().toByteArray());
            Block block = Block.createBlockWithCid(cid, b.getData().toByteArray());
            m.addBlock(block);
        }

        for (MessageOuterClass.Message.BlockPresence bi : pbm.getBlockPresencesList()) {
            Cid cid = new Cid(bi.getCid().toByteArray());
            if (!cid.isDefined()) {
                throw new RuntimeException("errCidMissing");
            }
            m.addBlockPresence(cid, bi.getType());
        }


        m.pendingBytes = pbm.getPendingBytes();

        return m;
    }

    public void addEntry(@NonNull Cid cid, int priority, boolean cancel,
                         @NonNull MessageOuterClass.Message.Wantlist.WantType wantType,
                         boolean sendDontHave) {
        Entry entry = wantlist.get(cid);
        if (entry != null) {
            // Only change priority if want is of the same type
            if (entry.WantType == wantType) {
                entry.priority = priority;
            }
            // Only change from "dont cancel" to "do cancel"
            if (cancel) {
                entry.Cancel = true;
            }
            // Only change from "dont send" to "do send" DONT_HAVE
            if (sendDontHave) {
                entry.SendDontHave = true;
            }
            // want-block overrides existing want-have
            if (wantType == MessageOuterClass.Message.Wantlist.WantType.Block
                    && entry.WantType == MessageOuterClass.Message.Wantlist.WantType.Have) {
                entry.WantType = wantType;
            }
            wantlist.put(cid, entry);
            return;
        }

        entry = new Entry();
        entry.cid = cid;
        entry.priority = priority;
        entry.WantType = wantType;
        entry.SendDontHave = sendDontHave;
        entry.Cancel = cancel;

        wantlist.put(cid, entry);

    }

    public Collection<Entry> wantlist() {
        return wantlist.values();
    }

    public Collection<Block> blocks() {
        return blocks.values();
    }

    private List<Cid> getBlockPresenceByType() {
        List<Cid> cids = new ArrayList<>();
        for (Map.Entry<Cid, MessageOuterClass.Message.BlockPresenceType> entry :
                blockPresences.entrySet()) {
            if (entry.getValue() == MessageOuterClass.Message.BlockPresenceType.Have) {
                cids.add(entry.getKey());
            }
        }
        return cids;
    }

    public List<Cid> haves() {
        return getBlockPresenceByType();
    }

    private int pendingBytes() {
        return pendingBytes;
    }

    public void entry(@NonNull Cid key, int priority,
                      @NonNull MessageOuterClass.Message.Wantlist.WantType wantType,
                      boolean sendDontHave) {
        addEntry(key, priority, false, wantType, sendDontHave);
    }

    public boolean empty() {
        return blocks.size() == 0 && wantlist.size() == 0 && blockPresences.size() == 0;
    }

    public void addBlock(@NonNull Block block) {
        blockPresences.remove(block.getCid());
        blocks.put(block.getCid(), block);
    }

    private void addBlockPresence(@NonNull Cid cid,
                                  @NonNull MessageOuterClass.Message.BlockPresenceType type) {
        if (blocks.containsKey(cid)) {
            return;
        }
        blockPresences.put(cid, type);
    }

    public void addHave(@NonNull Cid cid) {
        addBlockPresence(cid, MessageOuterClass.Message.BlockPresenceType.Have);
    }

    public void addDontHave(@NonNull Cid cid) {
        addBlockPresence(cid, MessageOuterClass.Message.BlockPresenceType.DontHave);
    }

    public void setPendingBytes(int pendingBytes) {
        this.pendingBytes = pendingBytes;
    }

    public MessageOuterClass.Message toProtoV1() {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        MessageOuterClass.Message.Wantlist.Builder wantBuilder =
                MessageOuterClass.Message.Wantlist.newBuilder();


        for (Entry entry : wantlist.values()) {
            wantBuilder.addEntries(entry.ToPB());
        }
        wantBuilder.setFull(full);
        builder.setWantlist(wantBuilder.build());


        for (Block block : blocks()) {
            builder.addPayload(MessageOuterClass.Message.Block.newBuilder()
                    .setData(ByteString.copyFrom(block.getData()))
                    .setPrefix(ByteString.copyFrom(block.getCid().getPrefix().bytes())).build());
        }


        for (Map.Entry<Cid, MessageOuterClass.Message.BlockPresenceType> mapEntry :
                blockPresences.entrySet()) {
            builder.addBlockPresences(MessageOuterClass.Message.BlockPresence.newBuilder()
                    .setType(mapEntry.getValue())
                    .setCid(ByteString.copyFrom(mapEntry.getKey().bytes())));
        }

        builder.setPendingBytes(pendingBytes());

        return builder.build();

    }

    // Entry is a wantlist entry in a Bitswap message, with flags indicating
    // - whether message is a cancel
    // - whether requester wants a DONT_HAVE message
    // - whether requester wants a HAVE message (instead of the block)
    static class Entry {
        public Cid cid;
        public int priority;
        public MessageOuterClass.Message.Wantlist.WantType WantType;
        public boolean Cancel;
        public boolean SendDontHave;

        // Get the entry in protobuf form
        public MessageOuterClass.Message.Wantlist.Entry ToPB() {

            return MessageOuterClass.Message.Wantlist.Entry.newBuilder().setBlock(
                            ByteString.copyFrom(cid.bytes())
                    ).setPriority(priority).
                    setCancel(Cancel).
                    setWantType(WantType).
                    setSendDontHave(SendDontHave).build();

        }
    }
}
