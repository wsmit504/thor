package threads.lite.bitswap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.Peer;
import threads.lite.core.Closeable;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.host.LiteHost;
import threads.lite.host.Session;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.TokenData;
import threads.lite.utils.DataHandler;


public class BitSwapManager implements BitSwap {

    private static final String TAG = BitSwapManager.class.getSimpleName();
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Session session;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final ConcurrentHashMap<Peer, QuicConnection> peers = new ConcurrentHashMap<>();
    @NonNull
    private final BitSwapEngine engine;

    public BitSwapManager(@NonNull LiteHost host,
                          @NonNull Session session,
                          @NonNull BlockStore blockStore) {
        this.host = host;
        this.session = session;
        this.blockStore = blockStore;
        this.engine = new BitSwapEngine(blockStore);
    }

    private static void writeMessage(@NonNull QuicConnection conn, @NonNull BitSwapMessage message) {

        if (IPFS.BITSWAP_SEND_REQUEST_ACTIVE) {
            try {
                conn.createStream(new StreamDataHandler(new TokenData() {
                            @Override
                            public void throwable(Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                                conn.close();
                            }

                            @Override
                            public void token(String token) throws Exception {
                                if (!Arrays.asList(IPFS.STREAM_PROTOCOL,
                                        IPFS.BITSWAP_PROTOCOL).contains(token)) {
                                    throw new Exception("Token " + token + " not supported");
                                }
                            }
                        }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                        .thenApply(quicStream -> quicStream.writeOutput(
                                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL))
                                .thenApply(stream -> stream.writeOutput(DataHandler.encode(message.toProtoV1()))
                                        .thenApply(QuicStream::closeOutput)));

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getClass().getSimpleName() +
                        " : " + throwable.getMessage());
            }
        }
    }

    public void haveReceived(@NonNull QuicConnection conn, @NonNull List<Cid> cids) {

        for (Cid cid : cids) {
            if (!blockStore.hasBlock(cid)) {
                sendWantsMessage(conn, Collections.singletonList(cid));
            }
        }
    }

    public void clear() {
        try {
            peers.values().forEach(QuicConnection::close);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            peers.clear();
        }
    }

    private void findProviders(@NonNull ExecutorService scheduler,
                               @NonNull AtomicBoolean findProviders,
                               @NonNull Closeable closeable,
                               @NonNull Cid cid) {
        scheduler.execute(() -> {

            long start = System.currentTimeMillis();
            try {
                LogUtils.debug(TAG, "Load Provider Start " + cid.String());

                if (closeable.isClosed()) {
                    return;
                }

                host.findProviders(closeable, (peer) -> {
                    if (peer.hasAddresses()) {

                        QuicConnection conn = peers.get(peer);
                        if (conn != null) {
                            if (!conn.isConnected()) {
                                peers.remove(peer);
                            } else {
                                // nothing to do here there
                                return;
                            }
                        }

                        // todo improve this connections async
                        if (!scheduler.isShutdown()) {
                            scheduler.execute(() -> {
                                try {
                                    synchronized (peer.getID()) {

                                        if (closeable.isClosed()) {
                                            return;
                                        }

                                        if (peers.containsKey(peer)) {
                                            return;
                                        }

                                        QuicConnection quicConnection = host.connect(
                                                session, peer,
                                                IPFS.CONNECT_TIMEOUT,
                                                IPFS.GRACE_PERIOD,
                                                IPFS.MAX_STREAMS,
                                                IPFS.MESSAGE_SIZE_MAX);


                                        LogUtils.debug(TAG, "New connection " +
                                                peer.getPeerId().toBase58() + " " +
                                                quicConnection.getRemoteAddress().toString());

                                        if (closeable.isClosed()) {
                                            return;
                                        }

                                        peers.put(peer, quicConnection);

                                    }

                                } catch (Throwable ignore) {
                                    // ignore
                                }
                            });
                        }
                    }
                }, cid);

                if (closeable.isClosed()) {
                    throw new InterruptedException();
                }

                if (!scheduler.isShutdown()) {
                    findProviders.set(false);
                }

            } catch (InterruptedException ignore) {
                // nothing to do here
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, "Load Provider Finish " + cid.String() +
                        " onStart [" + (System.currentTimeMillis() - start) + "]...");
            }
        });
    }

    public Block runWantHaves(@NonNull Closeable closeable, @NonNull Cid cid) throws InterruptedException {

        ExecutorService scheduler = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        AtomicBoolean findProviders = new AtomicBoolean(false);

        try {

            Set<QuicConnection> haves = new HashSet<>();

            while (!blockStore.hasBlock(cid)) {

                if (closeable.isClosed()) {
                    throw new InterruptedException();
                }

                for (QuicConnection conn : session.getSwarm()) {
                    if (!haves.contains(conn)) {
                        haves.add(conn);
                        sendHaveMessage(conn, Collections.singletonList(cid));
                    }
                }

                for (QuicConnection conn : host.getIncomingConnections()) {
                    if (!haves.contains(conn)) {
                        haves.add(conn);
                        sendHaveMessage(conn, Collections.singletonList(cid));
                    }
                }

                for (Peer peer : peers.keySet()) {
                    QuicConnection conn = peers.get(peer);
                    if (conn != null) {
                        if (!conn.isConnected()) {
                            peers.remove(peer);
                            continue;
                        }
                        if (!haves.contains(conn)) {
                            haves.add(conn);
                            sendHaveMessage(conn, Collections.singletonList(cid));
                        }
                    }
                }

                if (IPFS.BITSWAP_FIND_PROVIDERS_ACTIVE) {
                    try {
                        if (!findProviders.getAndSet(true)) {
                            findProviders(scheduler, findProviders, closeable, cid);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }
        } finally {
            scheduler.shutdown();
            scheduler.shutdownNow();
        }

        return blockStore.getBlock(cid);
    }

    public void blockReceived(@NonNull Block block) {

        Cid cid = block.getCid();
        if (!blockStore.hasBlock(cid)) {
            blockStore.putBlock(block);
        }
    }

    @Nullable
    public Block getBlock(@NonNull Closeable closeable, @NonNull Cid cid) throws InterruptedException {

        try {
            Block block = blockStore.getBlock(cid);
            if (block == null) {
                AtomicBoolean done = new AtomicBoolean(false);
                LogUtils.info(TAG, "Block Get " + cid.String());

                try {
                    return runWantHaves(() -> closeable.isClosed() || done.get(), cid);
                } finally {
                    done.set(true);
                }
            }
            return block;

        } finally {
            LogUtils.info(TAG, "Block Release  " + cid.String());
        }
    }


    public void receiveMessage(@NonNull QuicConnection conn, @NonNull BitSwapMessage bsm) {

        for (Block block : bsm.blocks()) {
            blockReceived(block);
        }
        haveReceived(conn, bsm.haves());

        engine.receiveMessage(conn, bsm);
    }

    void sendHaveMessage(@NonNull QuicConnection conn, @NonNull List<Cid> haves) {
        sendHaves(conn, haves);
    }

    private void sendHaves(@NonNull QuicConnection conn, @NonNull List<Cid> haves) {
        if (haves.size() == 0) {
            return;
        }

        int priority = Integer.MAX_VALUE;

        BitSwapMessage message = BitSwapMessage.create(false);

        for (Cid c : haves) {

            // Broadcast wants are sent as want-have
            MessageOuterClass.Message.Wantlist.WantType wantType =
                    MessageOuterClass.Message.Wantlist.WantType.Have;

            message.entry(c, priority, wantType, false);

            priority--;
        }

        if (message.empty()) {
            return;
        }

        writeMessage(conn, message);

    }

    void sendWantsMessage(@NonNull QuicConnection conn, @NonNull List<Cid> wants) {
        sendWants(conn, wants);
    }

    public void sendWants(@NonNull QuicConnection conn, @NonNull List<Cid> wants) {

        if (wants.size() == 0) {
            return;
        }
        BitSwapMessage message = BitSwapMessage.create(false);

        int priority = Integer.MAX_VALUE;

        for (Cid c : wants) {

            message.entry(c, priority,
                    MessageOuterClass.Message.Wantlist.WantType.Block, true);

            priority--;
        }

        if (message.empty()) {
            return;
        }

        writeMessage(conn, message);

    }
}
