package threads.lite.bitswap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.ImplementationError;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Closeable;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.TokenData;
import threads.lite.utils.DataHandler;


public final class BitSwapEngine implements BitSwap {
    public static final int MaxBlockSizeReplaceHasWithBlock = 1024;
    private static final String TAG = BitSwapEngine.class.getSimpleName();
    private final BlockStore blockstore;


    public BitSwapEngine(@NonNull BlockStore bs) {
        this.blockstore = bs;
    }

    @Nullable
    @Override
    public Block getBlock(Closeable closeable, Cid cid) throws InterruptedException {
        throw new ImplementationError();
    }

    @Override
    public void clear() {
        // nothing to do here
    }

    public void receiveMessage(@NonNull QuicConnection conn, @NonNull BitSwapMessage bsm) {

        if (IPFS.BITSWAP_SEND_REPLY_ACTIVE) {
            BitSwapMessage msg = messageReceived(bsm);
            if (!msg.empty()) {
                sendReply(conn, msg);
            }
        }
    }


    private void sendReply(@NonNull QuicConnection conn, BitSwapMessage msg) {
        try {
            conn.createStream(new StreamDataHandler(new TokenData() {
                        @Override
                        public void throwable(Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                            conn.close();
                        }

                        @Override
                        public void token(String token) throws Exception {
                            if (!Arrays.asList(IPFS.STREAM_PROTOCOL,
                                    IPFS.BITSWAP_PROTOCOL).contains(token)) {
                                throw new Exception("Token " + token + " not supported");
                            }
                        }
                    }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .thenApply(quicStream -> quicStream.writeOutput(
                                    DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL))
                            .thenApply(stream -> stream.writeOutput(
                                            DataHandler.encode(msg.toProtoV1()))
                                    .thenApply(QuicStream::closeOutput)));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @NonNull
    private BitSwapMessage createMessage(@NonNull List<Task> tasks) {

        // Create a new message
        BitSwapMessage msg = BitSwapMessage.create(false);

        // Amount of data in the request queue still waiting to be popped
        msg.setPendingBytes(0);

        // Split out want-blocks, want-haves and DONT_HAVEs
        List<Cid> blockCids = new ArrayList<>();
        Map<Cid, Task> blockTasks = new HashMap<>();

        for (Task task : tasks) {
            Cid c = task.cid;
            if (task.HaveBlock) {
                if (task.IsWantBlock) {
                    blockCids.add(c);
                    blockTasks.put(c, task);
                } else {
                    // Add HAVES to the message
                    msg.addHave(c);
                }
            } else {
                // Add DONT_HAVEs to the message
                msg.addDontHave(c);
            }
        }

        Map<Cid, Block> blks = getBlocks(blockCids);
        for (Map.Entry<Cid, Task> entry : blockTasks.entrySet()) {
            Block blk = blks.get(entry.getKey());
            // If the block was not found (it has been removed)
            if (blk == null) {
                // If the client requested DONT_HAVE, add DONT_HAVE to the message
                if (entry.getValue().SendDontHave) {
                    msg.addDontHave(entry.getKey());
                }
            } else {
                msg.addBlock(blk);
            }
        }
        return msg;

    }


    @NonNull
    private BitSwapMessage messageReceived(@NonNull BitSwapMessage bsm) {

        List<BitSwapMessage.Entry> wants = new ArrayList<>();
        for (BitSwapMessage.Entry et : bsm.wantlist()) {
            if (!et.Cancel) {
                wants.add(et);
            }
        }

        Set<Cid> wantKs = new HashSet<>();
        for (BitSwapMessage.Entry entry : wants) {
            wantKs.add(entry.cid);
        }

        HashMap<Cid, Integer> blockSizes = getBlockSizes(wantKs);

        List<Task> tasks = new ArrayList<>();

        for (BitSwapMessage.Entry entry : wants) {
            // For each want-have / want-block

            Cid c = entry.cid;
            Integer blockSize = blockSizes.get(entry.cid);

            if (blockSize == null) {
                LogUtils.debug(TAG, "Bitswap engine: block not found" +
                        " cid " + entry.cid.String() + " sendDontHave " + entry.SendDontHave);

                // Only add the task to the queue if the requester wants a DONT_HAVE
                if (IPFS.SEND_DONT_HAVES && entry.SendDontHave) {

                    boolean isWantBlock =
                            entry.WantType == MessageOuterClass.Message.Wantlist.WantType.Block;

                    Task task = new Task(c, false, isWantBlock, true);
                    tasks.add(task);

                }
            } else {
                boolean isWantBlock = sendAsBlock(entry.WantType, blockSize);

                LogUtils.debug(TAG, "Bitswap engine: block found" +
                        " cid " + entry.cid.String() + " isWantBlock " + isWantBlock);

                Task task = new Task(c, true, isWantBlock, entry.SendDontHave);
                tasks.add(task);
            }
        }
        return createMessage(tasks);
    }

    private boolean sendAsBlock(MessageOuterClass.Message.Wantlist.WantType wantType, Integer blockSize) {
        boolean isWantBlock = wantType == MessageOuterClass.Message.Wantlist.WantType.Block;
        return isWantBlock || blockSize <= MaxBlockSizeReplaceHasWithBlock;
    }


    public Map<Cid, Block> getBlocks(@NonNull List<Cid> cids) {
        Map<Cid, Block> blks = new HashMap<>();
        for (Cid c : cids) {
            Block block = blockstore.getBlock(c);
            if (block != null) {
                blks.put(c, block);
            }
        }
        return blks;
    }

    public HashMap<Cid, Integer> getBlockSizes(@NonNull Set<Cid> wantKs) {

        HashMap<Cid, Integer> sizes = new HashMap<>();
        for (Cid cid : wantKs) {
            int size = blockstore.getSize(cid);
            if (size > 0) {
                sizes.put(cid, size);
            }
        }
        return sizes;
    }


    private static class Task {
        // Topic for the task
        public final Cid cid;
        // Tasks can be want-have or want-block
        public final boolean IsWantBlock;
        // Whether to immediately send a response if the block is not found
        public final boolean SendDontHave;
        // Whether the block was found
        public final boolean HaveBlock;

        public Task(@NonNull Cid cid, boolean haveBlock, boolean isWantBlock, boolean sendDontHave) {
            this.cid = cid;
            this.SendDontHave = sendDontHave;
            this.IsWantBlock = isWantBlock;
            this.HaveBlock = haveBlock;
        }

    }


}
