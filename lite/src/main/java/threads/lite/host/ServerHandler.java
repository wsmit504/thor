package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;
import net.luminis.quic.server.ApplicationProtocolConnection;

import java.util.function.Consumer;

import threads.lite.LogUtils;

public class ServerHandler extends ApplicationProtocolConnection implements Consumer<QuicStream> {
    private static final String TAG = ServerHandler.class.getSimpleName();
    private final Session session;
    private final QuicConnection quicConnection;

    public ServerHandler(@NonNull Session session, @NonNull QuicConnection quicConnection) {
        this.session = session;
        this.quicConnection = quicConnection;
        quicConnection.setPeerInitiatedStreamCallback(this);
        session.getHost().incomingConnection(quicConnection);
        LogUtils.error(TAG, "Server connection established " +
                quicConnection.getRemoteAddress().toString());
    }

    @Override
    public void accept(QuicStream quicStream) {
        new StreamHandler(quicConnection, quicStream, session);
    }
}
