package threads.lite.host;

public interface TokenData {
    void throwable(Throwable throwable);

    void token(String token) throws Exception;

}
