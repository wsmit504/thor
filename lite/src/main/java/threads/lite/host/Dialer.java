package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.ImplementationError;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.TransportParameters;
import net.luminis.quic.Version;

import java.io.IOException;
import java.net.ConnectException;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public class Dialer {
    private static final String TAG = Dialer.class.getSimpleName();
    private static final AtomicInteger failure = new AtomicInteger(0);
    private static final AtomicInteger success = new AtomicInteger(0);


    @NonNull
    public static QuicConnection dial(@NonNull LiteHost host,
                                      @Nullable Session session,
                                      @NonNull PeerId peerId,
                                      @NonNull Multiaddr address,
                                      int timeout,
                                      int maxIdleTimeoutInSeconds,
                                      int initialMaxStreams,
                                      int initialMaxStreamData)
            throws ConnectException, InterruptedException {


        LiteHostCertificate selfSignedCertificate = host.getSelfSignedCertificate();

        long start = System.currentTimeMillis();
        boolean run = false;
        try {
            int initialMaxData = initialMaxStreamData;
            if (initialMaxStreams > 0) {
                initialMaxData = Integer.MAX_VALUE;
            }
            QuicClientConnectionImpl conn = QuicClientConnectionImpl.newBuilder()
                    .version(Version.IETF_draft_29) // in the future switch to version 1
                    .noServerCertificateCheck()
                    .clientCertificate(selfSignedCertificate.cert())
                    .clientCertificateKey(selfSignedCertificate.key())
                    .host(address.getHost())
                    .port(address.getPort())
                    .alpn(IPFS.ALPN)
                    .transportParams(new TransportParameters(
                            maxIdleTimeoutInSeconds, initialMaxData, initialMaxStreamData,
                            initialMaxStreams, 0))
                    .build();

            conn.connect(timeout);

            if (session != null) {
                conn.setPeerInitiatedStreamCallback(stream ->
                        new StreamHandler(conn, stream, session));
            } else {
                if (initialMaxStreams > 0) {
                    // session must be defined when initial max streams is greater 0
                    throw new ImplementationError();
                }
            }
            run = true;
            return conn;
        } catch (IOException e) {
            throw new ConnectException(e.getMessage());
        } finally {
            if (LogUtils.isDebug()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.debug(TAG, "Run dialClient " + run +
                        " Success " + success.get() +
                        " Failure " + failure.get() +
                        " Peer " + peerId.toBase58() + " " +
                        address + " " + (System.currentTimeMillis() - start));
            }
        }
    }

}
