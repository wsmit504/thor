package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.bitswap.BitSwap;
import threads.lite.bitswap.BitSwapManager;
import threads.lite.bitswap.BitSwapMessage;
import threads.lite.cid.Cid;
import threads.lite.core.Closeable;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;


public class Session {

    @NonNull
    private final BitSwap bitSwap;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Set<QuicConnection> swarm = ConcurrentHashMap.newKeySet();

    public Session(@NonNull BlockStore blockstore, @NonNull LiteHost host) {
        this.host = host;
        this.bitSwap = new BitSwapManager(host, this, blockstore);
    }

    public Session(@NonNull BitSwap bitSwap, @NonNull LiteHost host) {
        this.host = host;
        this.bitSwap = bitSwap;
    }

    @Nullable
    public Block getBlock(@NonNull Closeable closeable, @NonNull Cid cid)
            throws InterruptedException {
        return bitSwap.getBlock(closeable, cid);
    }

    public void clear(boolean clearSwarm) {
        if (clearSwarm) swarm.forEach(QuicConnection::close);
        bitSwap.clear();
        if (clearSwarm) swarm.clear();
    }

    public void receiveMessage(@NonNull QuicConnection conn, @NonNull BitSwapMessage bsm) {
        bitSwap.receiveMessage(conn, bsm);
    }

    @NonNull
    public LiteHost getHost() {
        return host;
    }


    public void swarmReduce(@NonNull QuicConnection connection) {
        swarm.remove(connection);
    }

    public boolean swarmEnhance(@NonNull QuicConnection connection) {
        return swarm.add(connection);
    }

    @NonNull
    public List<QuicConnection> getSwarm() {
        List<QuicConnection> result = new ArrayList<>();
        for (QuicConnection conn : swarm) {
            if (conn.isConnected()) {
                result.add(conn);
            } else {
                swarm.remove(conn);
            }
        }

        return result;
    }


    public boolean swarmContains(@NonNull QuicConnection connection) {
        return swarm.contains(connection);
    }


}

