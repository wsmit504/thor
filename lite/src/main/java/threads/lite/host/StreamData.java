package threads.lite.host;


import java.nio.ByteBuffer;

public interface StreamData extends TokenData {

    void data(ByteBuffer buffer) throws Exception;
}
