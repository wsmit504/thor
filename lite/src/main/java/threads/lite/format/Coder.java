package threads.lite.format;

import androidx.annotation.NonNull;

import java.security.MessageDigest;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.cid.Multihash;

public class Coder {
    @NonNull
    public static Merkledag.PBNode decode(@NonNull Block block) {
        Cid cid = block.getCid();
        if (cid.getType() != Cid.DagProtobuf) {
            throw new RuntimeException("only protobuf nodes supported");
        }
        try {
            return Merkledag.PBNode.parseFrom(block.getData());
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }

    }

    @NonNull
    public static Block encode(@NonNull Merkledag.PBNode node) {
        try {
            byte[] data = node.toByteArray();
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = Cid.encode(digest.digest(data), Multihash.Type.sha2_256.index);
            Cid cid = Cid.newCidV0(hash);
            return Block.createBlockWithCid(cid, data);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

}
