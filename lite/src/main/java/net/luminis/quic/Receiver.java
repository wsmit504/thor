/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import threads.lite.LogUtils;

/**
 * Receives UDP datagrams on separate thread and queues them for asynchronous processing.
 */
public class Receiver {
    public static final int MAX_DATAGRAM_SIZE = 1500;
    private static final String TAG = Receiver.class.getSimpleName();
    private static final AtomicInteger INSTANCES = new AtomicInteger(0);
    private final Consumer<Throwable> abortCallback;
    private final Consumer<RawPacket> consumer;
    private final Thread receiverThread;
    private final DatagramSocket socket;
    private final AtomicBoolean isClosing = new AtomicBoolean(false);


    public Receiver(DatagramSocket socket, Consumer<RawPacket> consumer,
                    Consumer<Throwable> abortCallback) {
        this.socket = socket;
        this.abortCallback = abortCallback;
        this.consumer = consumer;

        receiverThread = new Thread(this::run, "receiver-loop");
        receiverThread.setDaemon(true);
        receiverThread.setPriority(Thread.MAX_PRIORITY);
    }

    public void start() {
        receiverThread.start();
    }

    public void shutdown() {
        isClosing.set(true);
        receiverThread.interrupt();
    }


    private void run() {
        try {
            LogUtils.debug(TAG, "Instances " + INSTANCES.incrementAndGet());
            byte[] receiveBuffer = new byte[MAX_DATAGRAM_SIZE];
            while (!isClosing.get()) {
                DatagramPacket receivedPacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                try {
                    socket.receive(receivedPacket);
                    Instant timeReceived = Instant.now();
                    RawPacket rawPacket = new RawPacket(ByteBuffer.wrap(
                            receivedPacket.getData(), 0, receivedPacket.getLength()),
                            timeReceived, receivedPacket.getAddress(), receivedPacket.getPort());
                    consumer.accept(rawPacket);
                } catch (SocketTimeoutException timeout) {
                    // Impossible, as no socket timeout set
                    LogUtils.error(TAG, timeout);
                }
            }

            LogUtils.debug(TAG, "Terminating receive loop");
        } catch (IOException e) {
            if (!isClosing.get()) {
                // This is probably fatal
                LogUtils.error(TAG, "IOException while receiving datagrams", e);
                abortCallback.accept(e);
            } else {
                LogUtils.debug(TAG, "closing receiver");
            }
        } catch (Throwable fatal) {
            LogUtils.error(TAG, "IOException while receiving datagrams", fatal);
            abortCallback.accept(fatal);
        } finally {
            LogUtils.debug(TAG, "Instances " + INSTANCES.decrementAndGet());
        }
    }
}
