package net.luminis.quic;


public interface RawStreamData {

    byte[] getStreamData();

    boolean isFinal();
}
