package net.luminis.quic;

public class Settings {

    public final static int K_MAX_DATAGRAM_SIZE = 1200;           // TODO: 1200 is the minimum, actual value can be larger

    // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#appendix-B.1
    // "The RECOMMENDED value is the minimum of 10 * kMaxDatagramSize and max(2* kMaxDatagramSize, 14600))."
    // "kMaxDatagramSize: The RECOMMENDED value is 1200 bytes."
    public static final int INITIAL_WINDOW_SIZE = 10 * K_MAX_DATAGRAM_SIZE;
    // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#appendix-B.1
    // "Minimum congestion window in bytes.  The RECOMMENDED value is 2 * kMaxDatagramSize."
    public final static int K_MINIMUM_WINDOW = 2 * K_MAX_DATAGRAM_SIZE;


    // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#appendix-B.1
    // "Reduction in congestion window when a new loss event is detected.  The RECOMMENDED value is 0.5."
    public static final int K_LOSS_REDUCTION_FACTOR = 2; // note how it is used


    public static final int MAX_STREAMS = 10000;
    public static final int INITIAL_MAX_STREAM_DATA_SERVER = 1 << 22; // 4 MB
}
